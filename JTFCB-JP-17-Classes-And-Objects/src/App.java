class Person {

	// Instance variables (data or state)
	String name;
	int age;

	// Classes can contain:
	// 1. Data
	// 2. Subroutines (methods)
	void speak() {
		// trzy razy powie; nie musze okreslac i w srodku
		for(int i=0; i<3; i++){
		System.out.println("Hello. My name is " + name + ". I'm " + age + " years old.");
		}
	}

	void sayHello(){
		
		System.out.println("Hello there.");
	}
	
}

// class app
public class App {

	// subroutine (method) main
	public static void main(String[] args) {

		// object
		Person person1 = new Person();
		person1.name = "Joe Bloggs";
		person1.age = 37;
		person1.speak();
		// druga metoda
		person1.sayHello();

		Person person2 = new Person();
		person2.name = "Sarah Smith";
		person2.age = 20;
		person2.speak();
		// druga metoda
		person2.sayHello();
		
		System.out.println(person1.name);
	}

}
